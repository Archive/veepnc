/* 
 * Copyright (C) 2003 Red Hat Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * Author: Bastien Nocera <hadess@hadess.net>
 *
 */

#include "config.h"

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#include <gtk/gtk.h>
#include <glade/glade.h>
#include <glib/gi18n.h>

typedef struct Veepnc Veepnc;

struct Veepnc {
	GladeXML *xml;
	GtkWidget *win;

	GtkWidget *menu;
	GtkWidget *dropdown;

	char *ip;
	int pid;
};

static void update_configuration (Veepnc *vee, gboolean save);

static void
veepnc_error (char *msg, GtkWindow *parent)
{
	GtkWidget *error_dialog;

	error_dialog =
		gtk_message_dialog_new (parent,
				GTK_DIALOG_MODAL,
				GTK_MESSAGE_ERROR,
				GTK_BUTTONS_OK,
				"%s", msg);
	gtk_dialog_set_default_response (GTK_DIALOG (error_dialog),
			GTK_RESPONSE_OK);
	gtk_widget_show (error_dialog);
	gtk_dialog_run (GTK_DIALOG (error_dialog));
	gtk_widget_destroy (error_dialog);
	error_dialog = NULL;
}

static void
launch_vpnc (Veepnc *vee)
{
#if 0
	char *argv[1];

	if (g_file_test (EXECDIR"/vpnc", G_FILE_TEST_EXISTS) == FALSE)
	{
		//FIXME error
		return;
	}

	argv[0] = VPNC_PATH;
	argv[1] = NULL;

	//FIXME use the error
	if (g_spawn_async_with_pipes (NULL,	/* working_directory */
				argv,
				NULL,
				0,		/* flags */
				NULL,		/* child_setup */
				NULL,		/* user_data */
				&(vee->pid),
				NULL,		/* standard_input */
				NULL,		/* standard_output */
				NULL,		/* standard_error */
				NULL) == FALSE)
	{
		//FIXME error
	}
#endif
}

static void
stop_vpnc (Veepnc *vee)
{
	g_return_if_fail (vee->pid > 0);

	//FIXME
	if (kill (vee->pid, 9) == -1)
	{
		//FIXME
		return;
	}
}

static void
load_tun (void)
{
	if (g_spawn_command_line_sync ("/sbin/modprobe tun", NULL,
			NULL, NULL, NULL) == FALSE)
	{
		error (_("Could not load the tun driver needed for operation.\n"
				"Veepnc will now exit."), NULL);
		exit (1);
	}
}

#define SYSCONFIG_NETWORK_DIR "/etc/sysconfig/network-scripts/"

static gboolean
veepnc_check_device_is_vpn (const char *name, char **label)
{
	char *fname, *type, *location;
	char *contents, **lines;
	int i;
	gboolean retval;

	*label = NULL;
	type = NULL;
	retval = FALSE;
	location = NULL;
	fname = g_build_filename (SYSCONFIG_NETWORK_DIR, name, NULL);
	if (g_file_get_contents (fname, &contents, NULL, NULL) == FALSE) {
		g_message ("couldn't load file %s", name);
		g_free (fname);
		return FALSE;
	}

	lines = g_strsplit (contents, "\n", -1);
	g_free (contents);
	for (i = 0; lines[i] != NULL; i++) {
		if (g_str_has_prefix (lines[i], "DEVICETYPE=") != FALSE) {
			type = lines[i] + strlen ("DEVICETYPE=");
		} else if (g_str_has_prefix (lines[i], "LOCATION=") != FALSE) {
			location = lines[i] + strlen ("LOCATION=");
		}
	}
	if (type != NULL) {
		if (strcmp (type, "vpnc") == 0) {
			retval = TRUE;
		}
	}
	if (location != NULL) {
		*label = g_strdup (location);
	} else {
		*label = g_strdup (name + strlen ("ifcfg-"));
	}
	g_strfreev (lines);
	g_free (fname);
	return retval;
}

static void
veepnc_load_locations (void)
{
	const char *name;
	GDir *dir;
	char *label;

	dir = g_dir_open (SYSCONFIG_NETWORK_DIR, 0, NULL);
	if (dir == NULL)
		return;

	for (name = g_dir_read_name (dir); name != NULL; name = g_dir_read_name (dir)) {
		if (g_str_has_prefix (name, "ifcfg-") == FALSE)
			continue;

		if (veepnc_check_device_is_vpn (name, &label) == FALSE)
			continue;

		//FIXME add it to the interface
		g_print ("name %s label %s\n", name, label);
		g_free (label);
	}
}

static void
set_entry (Veepnc *vee, char *prefix, char *line, char *entry_name)
{
	GtkWidget *widget;
	char *item;

	item = line + strlen (prefix);

	if (entry_name == NULL)
	{
		vee->ip = g_strdup (item);
		return;
	}

	widget = glade_xml_get_widget (vee->xml, entry_name);
	if (widget == NULL)
		return;

	gtk_entry_set_text (GTK_ENTRY (widget), item);
}

static void
entry_changed (GtkEditable *editable, gpointer user_data)
{
	Veepnc *vee = (Veepnc *) user_data;

	update_configuration (vee, TRUE);
}

static void
setup_configuration (Veepnc *vee)
{
	GtkWidget *widget;
	char *contents, **lines;
	int i;

	/* No configuration */
	if (g_file_test ("/etc/vpnc.conf", G_FILE_TEST_EXISTS) == FALSE)
		return;

	if (g_file_get_contents ("/etc/vpnc.conf", &contents, NULL, NULL) == FALSE)
	{
		error (_("Could not read the configuration file "
					"'/etc/vpnc.conf'.\n"
					"Veepnc will now exit."), NULL);
		exit (1);
	}

	lines = g_strsplit (contents, "\n", 0);
	g_free (contents);

	for (i = 0; lines[i] != NULL; i++)
	{
		if (!strncmp (lines[i], "IPSec gateway ", strlen ("IPSec gateway ")))
			set_entry (vee, "IPSec gateway ", lines[i], NULL);
		if (!strncmp (lines[i], "IPSec ID ", strlen ("IPSec ID ")))
			set_entry (vee, "IPSec ID ", lines[i], "entry2");
		if (!strncmp (lines[i], "IPSec secret ", strlen ("IPSec secret ")))
			set_entry (vee, "IPSec secret ", lines[i], "entry3");
		if (!strncmp (lines[i], "Xauth username ", strlen ("Xauth username ")))
			set_entry (vee, "Xauth username ", lines[i], "entry1");
		if (!strncmp (lines[i], "Xauth password ", strlen ("Xauth password ")))
			set_entry (vee, "Xauth password ", lines[i], "entry4");
	}

	g_strfreev (lines);

	widget = glade_xml_get_widget (vee->xml, "entry2");
	g_signal_connect (G_OBJECT (widget), "changed",
			G_CALLBACK (entry_changed), vee);
	widget = glade_xml_get_widget (vee->xml, "entry3");
	g_signal_connect (G_OBJECT (widget), "changed",
			G_CALLBACK (entry_changed), vee);
	widget = glade_xml_get_widget (vee->xml, "entry1");
	g_signal_connect (G_OBJECT (widget), "changed",
			G_CALLBACK (entry_changed), vee);
	widget = glade_xml_get_widget (vee->xml, "entry4");
	g_signal_connect (G_OBJECT (widget), "changed",
			G_CALLBACK (entry_changed), vee);
}

static gboolean
not_empty_or_null (const char *str)
{
	if (str == NULL)
		return FALSE;
	if (strcmp (str, "") == 0)
		return FALSE;

	return TRUE;
}

#define NN(x) not_empty_or_null(x)

static void
update_configuration (Veepnc *vee, gboolean save)
{
#if 0
	GtkWidget *widget;
	const char *gateway, *id, *secret, *username, *password;

	widget = glade_xml_get_widget (vee->xml, "");
	gateway = vee->ip;

	widget = glade_xml_get_widget (vee->xml, "entry2");
	id = gtk_entry_get_text (GTK_ENTRY (widget));

	widget = glade_xml_get_widget (vee->xml, "entry3");
	secret = gtk_entry_get_text (GTK_ENTRY (widget));

	widget = glade_xml_get_widget (vee->xml, "entry1");
	username = gtk_entry_get_text (GTK_ENTRY (widget));

	widget = glade_xml_get_widget (vee->xml, "entry4");
	password = gtk_entry_get_text (GTK_ENTRY (widget));

	widget = glade_xml_get_widget (vee->xml, "okbutton1");

	if (NN(gateway) && NN(id) && NN(secret) && NN(username) && NN(password))
	{
		gtk_widget_set_sensitive (widget, TRUE);

		if (save == TRUE)
		{
			FILE *file;

			/* Only root can read that file */
			//FIXME that doesn't work
			umask (066);
			file = fopen ("/etc/vpnc.conf", "w+");
			if (file == NULL)
			{
				error (_("Could not open the configuration file"
						" '/etc/vpnc.conf' for writing."
						"\nMake sure you have the "
						"correct permissions on it."),
						NULL);
				exit (1);
			}

			fprintf (file, "IPSec gateway %s\n"
				 "IPSec ID %s\n"
				 "IPSec secret %s\n"
				 "Xauth username %s\n"
				 "Xauth password %s\n",
				 gateway, id, secret, username, password);

			fclose (file);
		}
	} else {
		gtk_widget_set_sensitive (widget, FALSE);
	}
#endif
}

int
main (int argc, char **argv)
{
	Veepnc *vee;
	int response;
	uid_t uid;

	gtk_init (&argc, &argv);

	veepnc_load_locations ();

	vee = g_new0 (Veepnc, 1);
	vee->xml = glade_xml_new ("./vpnc.glade", NULL, NULL);
	if (vee->xml == NULL)
	{
		char *filename;

		/* Try with the installed one */
		filename = g_build_filename (G_DIR_SEPARATOR_S, DATADIR,
				"veepnc", "vpnc.glade", NULL);
		vee->xml = glade_xml_new (filename, NULL, NULL);
		g_free (filename);
		if (vee->xml == NULL)
		{
			error (_("Couldn't load the main interface"
					" (vpnc.glade).\nMake sure that Veepnc"
					" is properly installed."), NULL);
			exit (1);
		}
	}

	vee->win = glade_xml_get_widget (vee->xml, "dialog1");
	vee->dropdown = glade_xml_get_widget (vee->xml, "optionmenu1");
	vee->ip = NULL;

	setup_configuration (vee);
	setup_gateways (vee);
	update_configuration (vee, FALSE);

	response = gtk_dialog_run (GTK_DIALOG (vee->win));

	if (response == GTK_RESPONSE_OK)
		launch_vpnc (vee);

	return 0;
}

